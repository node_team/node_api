let common = [
  "--publish-quiet",
  "--require-module ts-node/register",
  "--require ./features/**/*.ts",
  "--require ./features/*.ts", // Load custom formatter
  // '--format node_modules/cucumber-pretty' // Load custom formatter
].join(" ");

module.exports = {
  default: common,
};
