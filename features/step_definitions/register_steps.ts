import { Given, When, Then } from '@cucumber/cucumber'
import { assertThat } from "hamjest"
import request from 'supertest'
import app from '../../src/app'
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

let statusCode: any, body: any, data: any;

Given('a user provides firstname {string} lastname {string} email {string} password {string} and social medias accounts {string}', async function (firstName, lastName, email, password, social) {

    data = {
        firstName,
        lastName,
        email,
        password,
        social,
    }
    return data
});



When('user registers for an account', async function () {
    const response = await request(app).post('/api/v1/user/register').send(data);
    // console.log(response);
    statusCode = response.statusCode;

    body = response.body.status;

});



Then('A response is sent with status of {int} and message of {string}', function (int, string) {
    assertThat(statusCode, int);
    assertThat(body, string);
});





Given('a user enters firstname {string}, lastname {string} ,email {string}, password {string} and social medias accounts {string}', function (firstName, lastName, email, password, social) {
    data = {
        firstName,
        lastName,
        email,
        password,
        social
    }
    return data
});


When('registers using existing data', async function () {
    const response = await request(app).post('/api/v1/user/register').send(data);
    statusCode = response.statusCode;
    body = response.body.err.message;

});



Then('A response is sent with {int} and message of {string}', function (int, string) {
    assertThat(statusCode, int);
    assertThat(body, string);
});
