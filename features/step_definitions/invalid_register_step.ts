import { Given, When, Then } from '@cucumber/cucumber'
import { assertThat } from "hamjest"
import request from 'supertest'
import app from '../../src/app'
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

let statusCode: any, body: any, data: any;



Given('a user enters firstname {string}, lastname {string} ,email {string} password {string} and social media accounts <social>', async function (firstName, lastName, email, password) {

    data = {
        firstName,
        lastName,
        email,
        password,
    }
    return data;
});



Given('a user enters firstname {string}, lastname {string} ,email {int} password {string} and social media accounts <social>', function (firstName, lastName, email, password) {
    data = {
        firstName,
        lastName,
        email,
        password,
    }
    return data;
});

Given('a user enters firstname {int}, lastname {string} ,email {string} password {string} and social media accounts <social>', function (firstName, lastName, email, password) {
    data = {
        firstName,
        lastName,
        email,
        password,
    }
    return data;
});


Given('a user enters firstname {string}, lastname {int} ,email {string} password {string} and social media accounts <social>', function (firstName, lastName, email, password) {
    data = {
        firstName,
        lastName,
        email,
        password,
    }
    return data;
});


Given('a user enters firstname {string}, lastname {string} ,email {string} password {int} and social media accounts <social>', function (firstName, lastName, email, password) {
    data = {
        firstName,
        lastName,
        email,
        password,
    }
    return data;
});



When('a user registers using invalid data', async function () {
    const response = await request(app).post('/api/v1/user/register').send(data);
    //console.log(response);
    statusCode = response.statusCode;
    body = response.body.err.message;
});


Then('A response of invalid data is sent with {int} and message of {string}', function (int, string) {
    assertThat(statusCode, int);
    assertThat(body, string);
});
