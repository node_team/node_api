Feature: Invalid register

  Scenario: a user entering invalid data
    Given a user enters firstname <firstname>, lastname <lastname> ,email <email> password <password> and social media accounts <social>
    When a user registers using invalid data
    Then A response of invalid data is sent with <status> and message of <message>

    Examples: 
      | firstname   | lastname   | email           | password   | social social | status | message                                       |
      | 'firstname' | 'lastname' | ""              | "password" | " "           |    409 | "email is required"                           |
      | 'firstname' | 'lastname' |               7 | "password" | " "           |    409 | "email must be a string"                      |
      | 'firstname' | 'lastname' | "email"         | "password" | " "           |    409 | "must enter a valid email"                    |
      |          78 | 'Dexe'     | "mon@email.com" | "password" | " "           |    409 | "first name must be a string"                 |
      | ""          | 'babe'     | "mon@email.com" | "password" | " "           |    409 | "first name cannot be empty field"            |
      | "tes"       | 'babe'     | "mon@email.com" | "password" | " "           |    409 | "first name must be longer than 4 characters" |
      | "Dexe"      |         56 | "mon@email.com" | "password" | " "           |    409 | "last name must be a string"                  |
      | "Dexe"      | ''         | "mon@email.com" | "password" | " "           |    409 | "last name cannot be an empty field"          |
      | "test"      | 'lat'      | "mon@email.com" | "password" | " "           |    409 | "last name must be longer than 4 characters"  |
      | "test"      | 'Dexe'     | "mon@email.com" |         67 | " "           |    409 | "password must be a string"                   |
      | "rewq"      | 'babe'     | "mon@email.com" | ""         | " "           |    409 | "password is required"                        |
      | "test"      | 'babe'     | "mon@email.com" | "pass"     | " "           |    409 | "password must be longer than 6 characters"   |
