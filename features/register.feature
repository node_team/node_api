Feature: create user accounts

  Scenario: A user account succefully created
    Given a user provides firstname <firstname> lastname <lastname> email <email> password <password> and social medias accounts <social>
    When user registers for an account
    Then A response is sent with status of <status> and message of <message>

    Examples: 
      | firstname | lastname  | email            | password   | social                | status | message                        |
      | 'yehone'  | 'created' | "hfjd@email.com" | 'password' | '{"facebook": "acc"}' |    201 | "user registered successfully" |

  Scenario: a user entering already existing data
    Given a user enters firstname <firstname>, lastname <lastname> ,email <email>, password <password> and social medias accounts <social>
    When registers using existing data
    Then A response is sent with <status> and message of <message>

    Examples: 
      | firstname | lastname  | email            | password   | social                | status | message                               |
      | 'yehone'  | 'created' | "hfjd@email.com" | 'password' | '{"facebook": "acc"}' |    409 | "user with this email already exists" |
