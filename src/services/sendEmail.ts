import nodemailer from 'nodemailer';

export async function sendMail(to: string, subject: string, text: string) {
    return new Promise(async (resolve, reject) => {
        try {
            const testAccount = await nodemailer.createTestAccount();
            const transporter = nodemailer.createTransport({
                host: "smtp.ethereal.email",
                port: 587,
                secure: false,
                auth: {
                    user: testAccount.user,
                    pass: testAccount.pass,
                }
            })
            const result = await transporter.sendMail({
                from: 'test@gmail.com',
                to,
                subject,
                text,
            })
            console.log("Message sent: %s", result.messageId);
            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

            // Preview only available when sending through an Ethereal account
            console.log("Preview URL: %s", nodemailer.getTestMessageUrl(result));
            resolve(result);
        } catch (err) {
            reject({ status: 404, message: 'email not sent' });
        }

    })
}
