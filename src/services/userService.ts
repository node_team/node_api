import { PrismaClient } from "@prisma/client";
const bcrypt = require('bcryptjs');
import { generateHash, getUser } from "./auth";
import { createUserValidation, loginValidation, emailValidation, passwordValidation, resetValidation } from "../validator/validator";
import { mail } from "./mail";


const prisma = new PrismaClient();

export const create = async (user: any) => {
    return new Promise(async (resolve, reject) => {
        const validate = await createUserValidation.validate(user, { abortEarly: false });
        if (validate.error?.message) {
            reject({
                status: 400,
                message: validate.error?.message
            });
        }
        else {
            const hash: any = await generateHash(user.password).catch((err) => {
                reject(err)
            });
            const isEmail = await prisma.user.findUnique({
                where: { email: user.email }
            })
            if (isEmail) {
                reject({
                    status: 400,
                    message: "user with this email already exists"
                });
            }
            else {
                const result = await prisma.user.create({
                    data: {
                        firstname: user.firstName,
                        lastname: user.lastName,
                        email: user.email,
                        password: hash,
                    }
                })
                resolve({
                    status: 201,
                    message: "user created successfully"
                });
            }
        }


    })

}

export const login = async (email: string, password: string) => {
    return new Promise(async (resolve, reject) => {
        const validate = await loginValidation.validate({ email, password }, { abortEarly: false });
        if (validate.error?.message) {
            reject({
                status: 400,
                message: validate.error?.message
            });
        }
        else {
            await getUser(email).then((response: any) => {
                const isPass = response.password != null ? bcrypt.compareSync(password, response.password) : false;

                resolve({
                    status: isPass,
                    response
                });
            }).catch(err => {
                reject(err);
            });

        }

    })
}

export async function forgotService(email: string, task = "defaultMessage") {
    return new Promise(async (resolve, reject) => {
        const validate = await emailValidation.validate(email, { abortEarly: false });

        if (validate.error?.message) {

            reject({
                status: 400,
                message: validate.error?.message
            });
        }
        else {

            const user = await getUser(email).then(async (res) => {
                const response = await prisma.userReset.create({
                    data: {
                        email,
                    }
                }).catch((error) => {
                    reject(error);
                })
                const message = {
                    defaultMessage: "don't respond to this email if you did't request for account reset.",
                    SecurityIssue: "your are require to change your password there is security issue you are advised to change your password"
                }
                await mail(
                    email, // list of receivers
                    "reset your password", // Subject line
                    `<a href="http://localhost:3000/api/v1/user/reset/${response?.id}">http://localhost:3000/api/v1/user/reset/${response?.id}</a>
                    ${task == "SecurityIssue" ? message.SecurityIssue : message.defaultMessage}
                    `,
                )
                resolve(response);
            }).catch((error) => {
                reject(error);
            });
        }

    });
}




export async function resetService(id: string, password: string, confirmpassword: string) {
    return new Promise(async (resolve, reject) => {
        
        const userReset = await prisma.userReset.findUnique({
            where: {
                id,
            }
        }).catch((error)=>{
            reject(error)
         });
        if ( userReset != null && password == confirmpassword ) {

            const resetval = await resetValidation.validateAsync({password, confirmpassword}).catch((error: any) => {
            reject({
                message: error.message
            });
        })
            const email: string = userReset?.email || "";
            
            if (resetval != null && email!= null) {
                const user: any = await getUser(email).catch((error)=>{
                    console.log(error)
                 });
                // const email = user.email;
                const userId = user.id;
                let hash;
                await generateHash(password).then((response) => {
                    hash = response;
                });
                await prisma.user.update({
                    where: {
                        id: userId
                    },
                    data: {
                        password: hash
                    },
                })
                resolve({
                    message: 'user password successfully changed'
                })
            }
        }
         else if( userReset == null) {
            reject({
                message: 'make sure the reset link is correct'
            })
        } else if( password != confirmpassword ){
            reject({
                message: 'You entered different Password on password confirmation'
            })
        }
    })
}
