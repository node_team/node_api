import express, { Request, response, Response } from 'express';
import { generateHash } from '../services/auth';
import {
    resetService,
    create,
    forgotService,
    login
} from '../services/userService'



export const register = async (req: Request, res: Response) => {
    const {
        firstName,
        lastName,
        email,
        password
    } = req.body;

    const userData = {
        firstName,
        lastName,
        email,
        password,
    };
    const user: any = await create(userData).then(() => {
        res.status(201).json({
            status: "user registered successfully"
        });
    }).catch((err) => {
        res.status(409).json({
            err
        })
    });




};

export const loginUser = async (req: any, res: Response) => {
    const {
        username,
        password
    } = req.body;
    await login(username, password).then((response: any) => {
        const isMatch = response.status;
        if (isMatch == true) {
            const user = {
                id: response.id
            }
            if (req.isAuthenticated()) {
                res.status(200).send({
                    message: 'user logged in'
                })
            } else if (req.isUnauthenticated()) {
                req.login(user, function () {
                    if (req.isAuthenticated()) {
                        res.status(200).send({
                            message: 'user logged in successfully'
                        })
                    } else {
                        res.status(404).send({
                            message: 'account information incorrect'
                        })
                    }
                })
            }
        } else if (isMatch == false) {
            res.status(401).json(
                { err: { message: 'Incorrect password' } }
            );
        } else {
            res.status(401).json({
                message: response.message
            });
        }

    }

    ).catch((err) => {
        res.status(401).json({
            err
        })
    });


};
export const logoutUser = async (req: any, res: Response) => {
    if (req.isAuthenticated()) {
        req.logout(function (err: any) {
            if (err) {
                res.status(401).send(err)
            } else {
                res.send({
                    message: "user successfully logged out"
                })
            }

        });
    } else {
        res.send({
            message: "user not authenticated"
        })
    }
}
export async function forgotPassword(req: any, res: Response) {
    const { email } = req.body;
    await forgotService(email).then((response: any) => {
        res.status(200).json({
            message: `check your email '${email}' address to reset you password`
        })
    }).catch((error) => {
        res.status(404).send(error)
    });
}

export const resetPassword = async (req: any, res: Response) => {
    const { id } = req.params;
    const { password, confirmpassword } = req.body;
    await resetService(id, password, confirmpassword).then((response) => {
        res.status(200).json({ response });
    }).catch((error) => {
        res.status(400).json({ error });
    })

}